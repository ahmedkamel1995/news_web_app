import moment from "moment";
import {
  ON_FORM_CHANGE,
  FETCH_ARTICLES,
  FETCH_ARTICLES_FINISHED,
  TOGGLE_NEWS_QUICK_VIEW,
} from "./types";

const static_country_list = [
  { key: "ae", value: "United Arab Emirates" },
  { key: "ar", value: "Argentina" },
  { key: "at", value: "Austria" },
  { key: "be", value: "Belgium" },
  { key: "bg", value: "Bulgaria" },
  { key: "br", value: "Brazil" },
  { key: "ca", value: "Canada" },
  { key: "de", value: "Germany" },
  { key: "es", value: "Spain" },
  { key: "ru", value: "Russia" },
  { key: "us", value: "United States of America" },
];

export const initialState = {
  source_list: undefined,
  sources: undefined,
  country_list: static_country_list,
  country: undefined,
  from: moment(),
  to: moment(),
  everythingDS: undefined,
  everythingDSLoading: undefined,
  modalVisible: false,
  modalData: undefined,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ON_FORM_CHANGE:
      return {
        ...state,
        [action.name]: action.value,
      };

    case FETCH_ARTICLES:
      return {
        ...state,
        everythingDSLoading: true,
      };
    case FETCH_ARTICLES_FINISHED:
      return {
        ...state,
        everythingDSLoading: false,
        everythingDS: action.data,
      };

    case TOGGLE_NEWS_QUICK_VIEW:
      return {
        ...state,
        modalVisible: !state.modalVisible,
        modalData: action.article_data,
      };

    case "RESET_DATA":
      return initialState;

    default:
      return state;
  }
};
