import { takeLatest, select, put, all } from "redux-saga/effects";
import createApiUrl from "../Utilities/createApiUrl";
import { getRequest } from "../Utilities/httpRequests";
import { renameArrObjProps } from "../Utilities/renameProps";
import notification from "antd/lib/notification";

import { FETCH_SOURCES, FETCH_ARTICLES } from "./types";
import { onFormChange, fetchArticlesFinished } from "./actions";
import getParams from "../Utilities/getParams";

const mainSelector = ({ mainReducer }) => mainReducer;
const replacer = {
  id: "key",
  name: "value",
};

function* requestSources() {
  try {
    const apiUrl = createApiUrl({
      url: "sources",
    });

    let response = yield getRequest(apiUrl);
    response = yield response.json();

    if (response.status === "ok") {
      let result = renameArrObjProps(response.sources, replacer);
      result = result.slice(0, 10);
      yield put(onFormChange({ name: "source_list", value: result }));
      yield put(onFormChange({ name: "sources", value: result[0].key }));
    }
  } catch (error) {
    console.error("Error while fetching requestSources =>", error);
  }
}

function* requestArticles({ pathname }) {
  try {
    const { sources, country, from, to } = yield select(mainSelector);

    const url = pathname === "/" ? "everything" : "top-headlines";

    const validParams = getParams({
      sources,
      country,
      from,
      to,
    });

    const apiUrl = createApiUrl({
      url,
      params: {
        pageSize: 8,
        ...validParams,
      },
    });

    let response = yield getRequest(apiUrl);
    response = yield response.json();

    if (response.status === "ok") {
      yield put(fetchArticlesFinished(response.articles));
    } else {
      notification.error({
        message: response.status,
        description: response.message,
      });
    }
  } catch (error) {
    console.error("Error while fetching requestArticles =>", error);
    yield put(fetchArticlesFinished());
  }
}

export default function* mainSaga() {
  yield all([takeLatest(FETCH_SOURCES, requestSources)]);
  yield all([takeLatest(FETCH_ARTICLES, requestArticles)]);
}
