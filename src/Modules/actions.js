import {
  ON_FORM_CHANGE,
  FETCH_SOURCES,
  FETCH_ARTICLES,
  FETCH_ARTICLES_FINISHED,
  TOGGLE_NEWS_QUICK_VIEW,
} from "./types";

export const onFormChange = ({ name, value }) => ({
  type: ON_FORM_CHANGE,
  name,
  value,
});

export const fetchSources = () => ({
  type: FETCH_SOURCES,
});

export const fetchArticles = (pathname) => ({
  type: FETCH_ARTICLES,
  pathname,
});
export const fetchArticlesFinished = (data) => ({
  type: FETCH_ARTICLES_FINISHED,
  data,
});

export const toggleModal = (article_data) => ({
  type: TOGGLE_NEWS_QUICK_VIEW,
  article_data,
});
