import React from "react";
import Select from "antd/lib/select";

const { Option } = Select;

const RenderSelect = ({ options, width, ...props }) => {
  return (
    <Select style={{ ...styles }} {...props}>
      {options &&
        options.map((option) => (
          <Option key={option.key} value={option.key}>
            {option.value}
          </Option>
        ))}
    </Select>
  );
};

export default React.memo(RenderSelect);

const styles = {
  width: "230px",
};
