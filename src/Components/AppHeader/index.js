import React from "react";
import "antd/dist/antd.css";
import Layout from "antd/lib/layout";
import Menu from "antd/lib/menu";
import { useHistory, useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";

import { HeaderTitle, MenuItem } from "./styled";

const { Header } = Layout;

const AppHeader = () => {
  const dispatch = useDispatch();

  const history = useHistory();
  const location = useLocation();

  const handleClick = React.useCallback(
    (to) => () => {
      history.push(`/${to}`);
      dispatch({
        type: "RESET_DATA",
      });
    },
    [history, dispatch]
  );

  return (
    <Header style={{ ...headerStyles }}>
      <HeaderTitle onClick={handleClick("")} children="News" />
      <Menu theme="dark" mode="horizontal" selectedKeys={[location.pathname]}>
        <MenuItem onClick={handleClick("")} key="/" children="Home" />
        <MenuItem
          onClick={handleClick("headlines")}
          key="/headlines"
          children="Headlines"
        />
      </Menu>
    </Header>
  );
};

export default AppHeader;

const headerStyles = {
  display: "flex",
  justifyContent: "space-between",
};
