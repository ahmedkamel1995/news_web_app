import styled from "styled-components";
import Menu from "antd/lib/menu";

export const HeaderTitle = styled.h3`
  color: #fff;
  font-size: 2.5rem;
  font-weight: bold;
  cursor: pointer;
`;

export const MenuItem = styled(Menu.Item)`
  font-size: 1rem;
`;
