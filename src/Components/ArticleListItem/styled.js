import styled from "styled-components";

export const ArticleTitle = styled.h5`
  font-size: 1.5rem;
  font-weight: bold;
  cursor: pointer;
`;

export const ArticleContent = styled.p`
  font-size: 1rem;
  margin: ${({ margin }) => margin};
`;

export const ArticleImage = styled.img`
  display: inline-block;
  width: 300px;
  min-width: 300px;
  margin-inline-end: 15px;
  cursor: pointer;
`;
