import React, { useCallback } from "react";
import { useDispatch } from "react-redux";

import Flex from "../../Components/Flex";
import { toggleModal } from "../../Modules/actions";

import { ArticleTitle, ArticleContent, ArticleImage } from "./styled";

const ArticleListItem = ({ articleData }) => {
  const dispatch = useDispatch();
  const { title, urlToImage, description } = articleData;

  const handleOnClick = useCallback(() => {
    dispatch(toggleModal(articleData));
  }, [dispatch, articleData]);

  return (
    <Flex padding="5px" borderBottom>
      <ArticleImage
        src={urlToImage}
        alt="article-image"
        onClick={handleOnClick}
      />
      <div>
        <ArticleTitle children={title} onClick={handleOnClick} />
        <ArticleContent children={description} />
      </div>
    </Flex>
  );
};

export default ArticleListItem;
