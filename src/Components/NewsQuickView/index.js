import React from "react";
import Modal from "antd/lib/modal";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import Flex from "../Flex";
import { onFormChange, toggleModal } from "../../Modules/actions";
import {
  ArticleTitle,
  ArticleContent,
  ArticleImage,
} from "../ArticleListItem/styled";

const NewsQuickView = (props) => {
  const dispatch = useDispatch();

  const history = useHistory();

  const { modalVisible, modalData } = useSelector((state) => state.mainReducer);
  const { urlToImage, title, source, author, description } = modalData || {};

  const onCloseModal = React.useCallback(() => {
    dispatch(toggleModal());
  }, [dispatch]);

  const handleViewDetails = React.useCallback(() => {
    dispatch(onFormChange({ name: "modalVisible", value: false }));
    history.push("/details");
  }, [history, dispatch]);

  return (
    <Modal
      visible={modalVisible}
      width="70%"
      footer={null}
      closable={false}
      onCancel={onCloseModal}
    >
      <div className="bordered-container">
        <div>
          <Flex>
            <ArticleImage src={urlToImage} />
            <section>
              <ArticleTitle children={title} />
              <p>Source: {source && source.name}</p>
              <p>Author: {author}</p>
            </section>
          </Flex>
          <ArticleContent margin="10px 0" children={description} />
          <a onClick={handleViewDetails}>View Details</a>
        </div>
      </div>
    </Modal>
  );
};

export default NewsQuickView;
