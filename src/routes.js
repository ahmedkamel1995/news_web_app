import React from "react";
import { Route, Switch } from "react-router-dom";
import Spin from "antd/lib/spin";

const { Suspense, lazy } = React;

const Home = lazy(() => import("./Pages/Home"));
const Details = lazy(() => import("./Pages/Details"));
const PageNotFound = lazy(() => import("./Pages/PageNotFound"));

export default () => (
  <Suspense fallback={<Spin />}>
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/headlines" exact component={Home} />
      <Route path="/details" exact component={Details} />
      <Route path="*" component={PageNotFound} />
    </Switch>
  </Suspense>
);
