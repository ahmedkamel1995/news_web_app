import React from "react";
import { useSelector } from "react-redux";

import Flex from "../../Components/Flex";

import {
  ArticleImage,
  ArticleTitle,
  ArticleContent,
} from "../../Components/ArticleListItem/styled";

const DetailsPage = (props) => {
  const { modalData } = useSelector((state) => state.mainReducer);

  const { url, urlToImage, title, source, author, description, publishedAt } =
    modalData || {};

  console.log("modalData: ", modalData);

  return (
    <div className="details-container">
      <Flex>
        <ArticleImage src={urlToImage} />
        <section>
          <ArticleTitle children={title} />
          <p>Source: {source && source.name}</p>
          <p>Author: {author}</p>
          <p>Published: {publishedAt}</p>
          <a href={url} target="_blank">
            Open original post
          </a>
        </section>
      </Flex>
      <ArticleContent margin="10px 0" children={description} />
    </div>
  );
};

export default DetailsPage;
