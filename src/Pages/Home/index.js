import React from "react";
import { connect } from "react-redux";
import SearchInput from "antd/lib/input/Search";
import DatePicker from "antd/lib/date-picker";

import { isArrayHasData } from "../../Utilities/dataChecker";
import Flex from "../../Components/Flex";
import Select from "../../Components/Select";
import { SearchIcon, RenderEmpty, RenderSkeleton } from "./utils/styled";
import { mapStateToProps, mapDispatchToProps } from "./utils/selectors";
import NewsQuickView from "../../Components/NewsQuickView";

const { lazy, useEffect, useCallback } = React;
const ArticleListItem = lazy(() => import("../../Components/ArticleListItem"));

const HomePage = ({
  everythingDS,
  onFormChange,
  sources,
  source_list,
  fetchSources,
  country_list,
  country,
  from,
  to,
  fetchArticles,
  everythingDSLoading,
  history,
}) => {
  const path = history.location.pathname;

  // Fetch sources in initial component mounting
  useEffect(() => {
    if (!source_list) {
      fetchSources();
    }
  }, [source_list, fetchSources]);

  const handleSelectChange = useCallback(
    (name) => (value) => {
      onFormChange({ name, value });
    },
    [onFormChange]
  );

  const handleDateValueChange = useCallback(
    (name) => (date) => {
      onFormChange({ name, value: date });
    },
    [onFormChange]
  );

  return (
    <div className="home-container">
      <Flex column>
        <SearchInput placeholder="Search..." />

        <Flex align="center" justify="space-between" margin="15px 0">
          <Select
            placeholder="Select Source"
            options={source_list}
            value={sources}
            onChange={handleSelectChange("sources")}
          />
          <Select
            placeholder="Select Country"
            options={country_list}
            value={country}
            onChange={handleSelectChange("country")}
          />
          <DatePicker
            className="date-picker"
            value={from}
            onChange={handleDateValueChange("from")}
          />
          <DatePicker
            className="date-picker"
            value={to}
            onChange={handleDateValueChange("to")}
          />
          <SearchIcon onClick={() => fetchArticles(path)} />
        </Flex>

        <div className="bordered-container">
          <RenderSkeleton loading={everythingDSLoading} active avatar>
            {isArrayHasData(everythingDS) ? (
              everythingDS.map((item, index) => (
                <ArticleListItem key={index} articleData={item} />
              ))
            ) : (
              <RenderEmpty />
            )}
          </RenderSkeleton>
        </div>

        <NewsQuickView />
      </Flex>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
