import {
  onFormChange,
  fetchSources,
  fetchArticles,
} from "../../../Modules/actions";

export const mapStateToProps = ({ mainReducer }) => ({
  ...mainReducer,
});

export const mapDispatchToProps = (dispatch) => ({
  onFormChange: (params) => dispatch(onFormChange(params)),
  fetchSources: () => dispatch(fetchSources()),
  fetchArticles: (pathname) => dispatch(fetchArticles(pathname)),
});
