import styled from "styled-components";
import SearchOutlined from "@ant-design/icons/SearchOutlined";
import Empty from "antd/lib/empty";
import Skeleton from "antd/lib/skeleton";

export const SearchIcon = styled(SearchOutlined)`
  font-size: 20px;
`;

export const RenderEmpty = styled(Empty)`
  padding: 5%;
`;

export const RenderSkeleton = styled(Skeleton)`
  padding: 3%;
`;
