import { combineReducers } from "redux";

import mainReducer from "../../Modules/reducer";

export default combineReducers({
  mainReducer,
});
