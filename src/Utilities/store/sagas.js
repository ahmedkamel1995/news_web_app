import { fork, all } from "redux-saga/effects";

import mainSaga from "../../Modules/saga";

export default function* rootSaga() {
  yield all([fork(mainSaga)]);
}
