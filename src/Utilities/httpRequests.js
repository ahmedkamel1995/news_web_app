import { BASE_URL, API_KEY } from "./constants";

export const getRequest = async (url, headers = null) => {
  return await fetch(BASE_URL + url, {
    headers: {
      "X-Api-Key": API_KEY,
      ...headers,
    },
  });
};
