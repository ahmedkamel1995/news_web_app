import moment from "moment";

export default (obj) => {
  let params = {};

  for (const [key, value] of Object.entries(obj)) {
    if (value) {
      if (value instanceof moment) {
        params = { ...params, [key]: value.format("YYYY-MM-DD") };
      } else {
        params = { ...params, [key]: value };
      }
    }
  }
  return params;
};
