export const isArrayHasData = (arr) => Array.isArray(arr) && !!arr.length;
