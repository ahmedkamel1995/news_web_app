export const renameObjProps = (obj = null, replacers = null) => {
  const isThereData = Boolean(obj);
  const isTherereplacers = Boolean(replacers);
  let finalObj = null;
  if (isThereData && isTherereplacers) {
    const replacerKeys = Object.keys(replacers);
    replacerKeys.forEach((key) => {
      if (obj.hasOwnProperty(key)) {
        finalObj = { ...finalObj, [replacers[key]]: obj[key] };
      }
    });
  }
  return finalObj;
};

export const renameArrObjProps = (arr, replacers) => {
  let finalArr = [];
  if (Array.isArray(arr) && arr.length > 0) {
    arr.forEach((objItem) => {
      const objWithNewProps = renameObjProps(objItem, replacers);
      finalArr = [...finalArr, objWithNewProps];
    });
    return finalArr;
  }
  return finalArr;
};
