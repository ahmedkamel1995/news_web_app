import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import AppRouter from "./routes";
import AppHeader from "./Components/AppHeader";
import store from "./Utilities/store";

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter basename="/">
        <AppHeader />
        <AppRouter />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
